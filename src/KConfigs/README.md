NOTE: the allnoconfig is simple, doesn't even have:

- SMP
- Preemption

NOTE: alldefconfig:

- no preemption but with SMP

## Custom allnoconfig + alldefconfig options

```
[*] 64-bit kernel

-> General setup
  -> Configure standard kernel features
[*] Enable support for printk

-> General setup
[*] Initial RAM filesystem and RAM disk (initramfs/initrd) support

-> Executable file formats / Emulations
[*] Kernel support for ELF binaries
[*] Kernel support for scripts starting with #!

-> Device Drivers
  -> Character devices
[*] Enable TTY

-> Device Drivers
  -> Character devices
    -> Serial drivers
[*] 8250/16550 and compatible serial support
[*]   Console on 8250/16550 and compatible serial port

-> File systems
  -> Pseudo filesystems
[*] /proc file system support
[*] sysfs file system support

# Memorizer depends KASAN depends sparsemem_vmemmap

-> Processor type and features
  [*] Sparse Memory virtual memmap

-> Kernel hacking 
  -> Memory Debugging 
    [*] KASan: runtime memory debugger

-> Kernel hacking
  -> Compile-time checks and compiler options
    [*] Debug Filesystem

-> Kernel hacking 
  -> Memory Debugging 
    [*] Memorizer: kernel object lifetime access tracing

# FTRACE: Only enabled for defconfig. noconfig fails to boot
-> Kernel hacking
  -> Tracers 
    [*] Kernel Function Tracer
    [*]   Kernel Function Graph Tracer 
    [ ] hnable/disable function tracing dynamically

```

## Memorizer Config

Used to enable the memorizer tracing infrastructure.
