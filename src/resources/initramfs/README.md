# Setting up an initramfs

This is how to create an initramfs for the minimal
configuration. For more complex kernel configs this may or
may not work.

The script is as follows:

```
$ curl https://busybox.net/downloads/busybox-1.26.2.tar.bz2 | tar xjf -
$ cd $TOP/busybox-1.26.2
$ mkdir -pv ../obj/busybox-x86
$ make O=../obj/busybox-x86 defconfig
$ make O=../obj/busybox-x86 menuconfig
$ cd ../obj/busybox-x86
$ make -j2
$ make install
$ mkdir -pv $TOP/initramfs/x86-busybox
$ cd $TOP/initramfs/x86-busybox
$ mkdir -pv {bin,sbin,etc,proc,sys,usr/{bin,sbin}}
$ cp -av $TOP/obj/busybox-x86/_install/* .
$ vim init
$ chmod +x init
$ find . -print0 \
  | cpio --null -ov --format=newc \
  | gzip -9 > $TOP/obj/initramfs-busybox-x86.cpio.gz
```

Busybox config:

```
-> Busybox Settings
  -> Build Options
    [x] Build BusyBox as a static binary (no shared libs)
```

init:
```
#!/bin/sh

mount -t proc none /proc
mount -t sysfs none /sys
mount host-code -t 9p /mnt/host
mount -t debugfs nodev /sys/kernel/debug

echo -e "\nBoot took $(cut -d' ' -f1 /proc/uptime)
seconds\n"

exec /bin/sh
```
