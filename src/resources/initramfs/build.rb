# Build the busybox image and init ramfs
bb = "busybox-1.26.2"
busyboxtop = Dir.pwd
busyboxsrc = busyboxtop+"/"+bb
busyboxobj = busyboxtop+"/obj/busybox-x86"
busyboxcfg = busyboxtop+"/busy-box-def-static.config"
bbinitramdisk = busyboxobj + "/initramfs-busybox-x86.cpio.gz"
init_script = busyboxtop+"/init-shell"
system("mkdir -pv #{busyboxtop}")
Dir.chdir(busyboxtop) do
  if not Dir.exist?(busyboxsrc)
    system("curl https://busybox.net/downloads/#{bb}.tar.bz2 | tar xjf -")
  end
  Dir.chdir(busyboxsrc) do
    system("mkdir -pv #{busyboxobj}")
    system("cp "+busyboxcfg+" "+busyboxobj+"/.config")
    system("make -j64 O=#{busyboxobj}")
    system("make O=#{busyboxobj} install")
  end
  system("mkdir -pv #{busyboxtop}/x86-busybox")
  Dir.chdir(busyboxtop+"/x86-busybox") do
    for dir in ["bin", "sbin", "etc", "proc", "sys", "usr", "mnt/host"]
        `mkdir -pv #{dir}`
    end
    system("cp -av #{busyboxobj}/_install/* .")
    system("cp #{init_script} init")
    system('chmod +x init')
    system("find . -print0 | cpio --null -ov --format=newc | gzip -9 > #{bbinitramdisk}")
  end
end
