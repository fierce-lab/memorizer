## Resources
- [KConfigs](./KConfigs): Kernel configuration files for plain Linux, KASAN, and Memorizer

- [bzImages](./bzImages): Pre-build kernel for plain Linux, KASAN, and Memorizer


## How to reproduce LMBench results in the [paper](https://dl.acm.org/doi/abs/10.1145/3456727.3463767)
With the [bzImage](./bzImages), we can boot the QEMU with [debian image](https://hub.docker.com/layers/160065030/brucechien/memorizer/uscope/images/sha256-77604891280ea9c43b498e439a744accdb253276f44daf6b21e6f6b4883c3050?context=repo) and run the LMBench inside the debian image.

The evaluation for LMBench has two parts. The first part is to test the performance of Linux, KASAN, Memorizer, and Memorizer with stack-trace enabled.
The second part is to test how Memorizer performs in multi-core setting.

To fast reproduce the result, we recommand using the [run.sh](../../run.sh) and choose to pass the customized [kernel path](./bzImages) and using the debian image to boot. We detail the sequence to get the LMBench results after logging into the QEMU as follows:

###  LMBench for different kernel configuration
- For Linux and KASAN
    - Run LMBench
        - ```cd /home/nick/lmbench```
        - ```make rerun``` or ```make results see```(to reconfigure LMBench to your env)
    - Copy the result
        - In host machine (provided the QEMU open the port 4441 for network): ```scp -P 4441 -i ../resources/.ssh/id_rsa root@localhost:/home/nick/lmbench/ ./```

- For Memorizer
    - Enable Memorizer
        - Memorizer:
            - ```echo 1 > /sys/kernel/debug/memorizer/memorizer_enabled```
            - ```echo 1 > /sys/kernel/debug/memorizer/memorizer_log_access```
            - ```echo 1 > /sys/kernel/debug/memorizer/cfg_log_on```
        - Memorizer with stack-trace:
            - ```echo 1 > /sys/kernel/debug/memorizer/memorizer_enabled```
            - ```echo 1 > /sys/kernel/debug/memorizer/memorizer_log_access```
            - ```echo 1 > /sys/kernel/debug/memorizer/stack_trace_on```
    - Run LMBench
        - ```cd /home/nick/lmbench```
        - ```make rerun see``` or ```make results see```(to reconfigure LMBench to your env)

    - Disable Memorizer
        - Memorizer:
            - ```echo 0 > /sys/kernel/debug/memorizer/memorizer_enabled```
            - ```echo 0 > /sys/kernel/debug/memorizer/memorizer_log_access```
            - ```echo 0 > /sys/kernel/debug/memorizer/cfg_log_on```
        - Memorizer with stack-trace:
            - ```echo 0 > /sys/kernel/debug/memorizer/memorizer_enabled```
            - ```echo 0 > /sys/kernel/debug/memorizer/memorizer_log_access```
            - ```echo 0 > /sys/kernel/debug/memorizer/stack_trace_on```
        - Copy the result
            - In host machine (provided the QEMU open the port 4441 for network): ```scp -P 4441 -i ../resources/.ssh/id_rsa root@localhost:/home/nick/lmbench/results/summary.out ./```

### LMBench fo SMP (Different core numbers)
For SMP evaluation, we're only interested in Memorizer kernel. To reproduce the result, the fatest way is to use the [run.sh](../../run.sh) and pass different SMP numbers in the set-up. After set-up the QEMU SMP core numbers, use the following steps to get the LMBench result:
- Enable Memorizer
    - ```echo 1 > /sys/kernel/debug/memorizer/memorizer_enabled```
    - ```echo 1 > /sys/kernel/debug/memorizer/memorizer_log_access```
    - ```echo 1 > /sys/kernel/debug/memorizer/cfg_log_on```

- Run LMBench
    - ```cd /home/nick/lmbench```
    - ```make rerun``` or ```make results see```(to reconfigure LMBench to your env)

- Disable Memorizer
    - ```echo 0 > /sys/kernel/debug/memorizer/memorizer_enabled```
    - ```echo 0 > /sys/kernel/debug/memorizer/memorizer_log_access```
    - ```echo 0 > /sys/kernel/debug/memorizer/cfg_log_on```


- Copy the result
    - In host machine (provided the QEMU open the port 4441 for network): ```scp -P 4441 -i ../resources/.ssh/id_rsa root@localhost:/home/nick/lmbench/results/summary.out ./```

### How to reproudce Phoronix results
To run the Phoronix test suites, follow the below steps and replace the ```encode-mp3``` to the test-suite you want to run.
- Enable Memorizer
    - ```echo 1 > /sys/kernel/debug/memorizer/memorizer_enabled```
    - ```echo 1 > /sys/kernel/debug/memorizer/memorizer_log_access```
    - ```echo 1 > /sys/kernel/debug/memorizer/cfg_log_on```

- Run Phoronix suite
    - ```phoronix-test-suite install-test encode-mp3```
    - ```phoronix-test-suite batch-run encode-mp3 > /root/encode-mp3-result```(switch encode-mp3 to your own phoronix test suite)

- Disable Memorizer
    - ```echo 0 > /sys/kernel/debug/memorizer/memorizer_enabled```
    - ```echo 0 > /sys/kernel/debug/memorizer/memorizer_log_access```
    - ```echo 0 > /sys/kernel/debug/memorizer/cfg_log_on```

- Copy the result
    - In host machine (provided the QEMU open the port 4441 for network): ```scp -P 4441 -i ../resources/.ssh/id_rsa root@localhost:/root/phoronix_result ./```

### How to reproduce LTP results

We use LTP for gaining large coverage of kernel behavior. As per the [LTP Repository](https://github.com/linux-test-project/ltp):


> Linux Test Project is a joint project started by SGI, OSDL and Bull developed and maintained by IBM, Cisco, Fujitsu, SUSE, Red Hat, Oracle and others. The project goal is to deliver tests to the open source community that validate the reliability, robustness, and stability of Linux.


We have a precompiled and built LTP on the Debian image. Please see the instructions on the LTP repo for general usage. Note that for memorizer you will need to run tests individually so that memory consumptions stays within the preallocated limits. 

For our evaluation in Memorizer, we do not evaluate performance and only use the LTP test suite to collect data and evidence about log size and coverage statistics.

**NOTE: the results from the paper included over 8 months of CPU tracing time and will not be recreatable in a short amount of time**

Specific script for our tests will be coming shortly...

## Use [run.sh](../../run.sh) to *attempt* automated SYSTOR evaluation
Example usage of how to run run.sh to get Phoronix encode-mp3 result:
```
Choose to run in background or pop up a shell
1) Background mode: automatic output cmap files and run uscope analysis
2) Shell mode: Pop up shell for running Memorizer on QEMU
> 1
```

```
Choose Memorizer kernel build method:
1) Build Memorizer kernel from Linuxkit
2) Build Memorizer kernel from source code
3) Use customized kernel
> 3
Customized kernel path: ./src/evaluation/bzImages/Memorizer-kernel
```

```
Choose root file system:
1) Disk image (ex: qcow2)
2) Initramfs (ex : cpio.gz)
> 1
```

```
Please input the disk image file path: (default is debian.qcow2)
> enter
```

```
Set QEMU boot configuration:
Set QEMU memory, default (32GB) press enter or enter a number (GB) > enter
Set Memorizer memory, default (20GB) press enter or enter a number (GB) > enter
Set core numbers, default (single core) press enter or enter a number > enter
```

```
Memorizer tracing config (default is 3):
1) Alloc
2) Alloc + Access
3) Alloc + Access + Call
4) Alloc + Access + Stack-trace
5) None
> 3
```

```Enter command to run, for example:
* bash command
* ltp
* phoronix
* lmbench
> phoronix
```

```
List Phoronix test cases or run Phoronix test case                   
1) Run Phoronix test case                                       
2) list LPhoronixTP test cases 
```

```
Enter Phoronix test case to run: > encode-mp3
```

- Get the ouptut at src/resource/phoronix_results or src/resource/lmbench_results
