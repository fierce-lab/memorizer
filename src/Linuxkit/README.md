### Resources
- [memorizer.yml](./memorizer.yml):Linuxkit build
  configuration file.

- [run\_memorizer.sh](./run_memorizer.sh): Example script
  for running Memorizer with QEMU.

### Linuxkit tutorial
Linuxkit is a utility for building and distributing specialized Linux distributions, and it is the fastest way to get
started with Memorizer. You can get a Memorizer image up and running quickly by following these commands.

## Step 0: (Optional) QEMU Install
To run the custom Memorizer Linux distribution, you will need an emulator or virtualization
platform capable of running Memorizer images.

We recommend using [QEMU](https://qemu.org/download).

## Step 1: Docker Installation
Linuxkit requires
[Docker](https://docs.docker.com/get-docker/) to be installed on the host machine that will be building
the Memmorizer image.


## Step 2: Linuxkit Installation
Clone the latest version of Linuxkit, found here:
[Linuxkit](github.com/linuxkit/linuxkit)

Follow the installation instructions in the project's documentation. Once Linuxkit is built, be sure to add it to your path.

# Step 3: Pull Memorizer Linuxkit Image
With Linuxkit installed, run the following commands to pull the Memorizer image.
```
mkdir -p build
<path to linuxkit build>/linuxkit build -dir build memorizer.yml
```

## Step 3: Run Memorizer
To run Memorizer in qemu, run the following script:
```
./run_memorizer
```

Allow qemu to run for a few minutes. It's normal for Memorizer to take
awhile to boot.

Optionally you can run the kernel in your preferred virtualization / emulation
software. Be sure to supply it with the same environment variables as used in
the script.

Be sure to provide ample memory for Memorizer when provisioning it as a guest. Failure to give the kernel a large amount of memory can lead
to the kernel failing to boot.

Note: Linuxkit only outputs a kernel image. We also provide a
simple example of initramfs, initramfs-busybox-x86.cpio.gz
as root file system for booting. 
