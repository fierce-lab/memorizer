## Resources
- [evaluation](./evaluation): Tutorial for how to reproduce the evaluation in the paper. Include pre-build kernels and kernel configs.

- [KConfigs](./KConfigs): Examples for kernel configuration
  files.

- [Linuxkit](./Linuxkit): Environment for fast-building the Linux
  kernel.

- [memorizer.patch](./memorizer.patch): Patch file of
  Memorizer based on Linux kernel version 4.10.

- [post-analysis](./post-analysis): Python script for
  parsing Memorizer output data.

- [resources](./resources): Directory for Memorizer resources like initramfs
  and where the run.sh(../run.sh) stores all the related resources like debian image,
  linux repo, compiled kernel, and etc.

# How do I run Memorizer?
There are two options to run Memorizer. If you're looking for a fast and easy
way to run the kernel, we recommend starting with our [Linuxkit Tutorial](./Linuxkit)

### How do I build Memorizer from source?
You can build Memorizer kernel from source in two ways. In this repository,
we have included a tarball of the kernel source. This can be compiled like
any other Linux kernel. Alternatively, we have included a patch file one can
apply to Linux4.10.0

### How do I patch Memorizer based on Linux4.10.0?
```
> cd $LINUX_SOURCE_PATH
> git checkout c470abd4fde40ea6a0846a2beab642a578c0b8cd
> git apply memorizer.patch
```

### How do I compile and config Memorizer?
```
> make menuconfig
```
Memorizer kernel configuration:
- CONFIG\_MEMORIZER: Enable Memorizer and depends on KASAN.
- CONFIG\_MEMORIZER\_STATS: Enable statistic summary for
Memorizer which will slow down the performance. Default is
off.

### What boot-time options does Memorizer provide?
- memorizer\_enalbed\_boot=yes/no: Enable/Disable alloc
  tracing at boot-time.

- mem\_log\_boot=yes/no: Enable/Disable access tracing at
  boot-time.

- cfg\_log\_boot=yes/no: Enable/Disable call tracing at
  boot-time.

- stack\_trace\_boot=yes/no: Enable/Disable stack tracing at
  boot-time.

- memalloc\_size=[number]: Specify how much memory assigned
  to Memorizer, default is 3 GB.

### How do I run Memorizer on QEMU?
We provide an example [script](./Linuxkit/run_memorizer.sh)
where you can change the KERNEL and INITRD variable to your
own compiled Linux kernel and preferred root file system. 

### How do I enable Memorizer at runtime?
In debugfs(/sys/kernel/debug/memorizer), we export
interfaces to configure Memorizer from the user-space.

- Object tracing:
```
> echo 1 > memorizer_enabled
```
- Access tracing:
```
> echo 1 > memorizer_log_access
```

- Call tracing:
To enable function call tracing without stack frame object tracing
```
> echo 1 > cfg_log_on
```

To enable function call tracing and stack frame object tracing
```
> echo 1 > stack_trace_on
```

- Clear freed shadow objects from output list
```
> echo 1 > clear_dead_kobjs
```

- Clear printed shadow objects
```
> cat clear_printed_lists
```

- Print live shadow objects
```
> cat print_live_objs
```

- Print global variables
```
> cat global_table
```

* Note: cfg\_log\_on and stack\_trace\_on are exclusive to
* each other. When switching between different call tracing,
* please disable the function trace first then ```echo 0 >
* cfgmap``` before turn on the other call tracing feature. 

Detailed documentation can be found
[here](https://gitlab.com/fierce-lab/linux-memorizer/-/blob/memorizer/Documentation/memorizer.txt)

### Memorizer output data
Memorizer exports files in debugfs file system and can be located
at path /sys/kernel/debug/memorizer/. For detailed output
format, please refer to [Memorizer
documentation](https://gitlab.com/fierce-lab/linux-memorizer/-/blob/memorizer/Documentation/memorizer.txt)

- kmap: Object tracing and access tracing data

- cfgmap: Call tracing data

- show\_stats: Statistic summary of Memorizer

With the output data, we provide a [Python
parser](./post-analysis/CAPMAP.py) which reads the vmlinux
and cmap (which is the concatenation of kmap and cfgmap file) 
files and output a CAPMAP graph for subject and
object access pattern.

### How to reproduce the evaluation results in the paper
Detail information can be found in this [README.md](./evaluation/README.md). It provide the kernels and kernel configs. To fast reproduce the result, one can pass the kernel to the [run.sh](../run.sh) and use the debian image which has the LMBench, LTP, and Phoronix installed to boot and get the evaluation results.