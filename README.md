# Memorizer

<!-- Trigger our visits counter by visiting countapi -->
<script async src="https://api.countapi.xyz/hit/fierce-lab.gitlab.io/memorizer-repo"></script>

### ACM Paper, Presentation, and Landing Page
[Lossless Instruction-to-Object Memory Tracing in the Linux Kernel](https://dl.acm.org/doi/abs/10.1145/3456727.3463767)

[Systor 2021 Presentation](https://www.youtube.com/watch?v=btVCQcQo8Ic) and [Slide](./slide/memorizer-systor21.pdf)

[Memorizer Landing Page](https://fierce-lab.gitlab.io/memorizer/)

### Repository Structure

This repo contains code, documents and tools pertaining to
the Memorizer kernel.
```
.
├── README.md                        This document.
├── css 
├── dashboard                        Memorizer Visualization Dashboard: Explore and visualize Memorizer data
├── index.html                       Memorizer landing page
├── run.sh                           Shell script to quick test and reproduce Memorizer data and analysis
├── slide                            Presentation slide
├── src
│   ├── KConfigs                     Configuration files for compiling the Memorizer kernel
│   │   ├── README.md
│   │   ├── deb.config
│   │   ├── mem-alldefconfig.config
│   │   ├── mem-allnoconfig.config
│   │   ├── memorizer_config.config
│   │   └── memorizer_debian.config
│   ├── evaluation                   Evaluation reproduce resources
│   │   ├── README.md                Tutorial to get started run eval
│   │   ├── bzImages                 bzImages for different kernel configs
│   │   └── Kconfigs                 Different kernel configs
│   ├── Linuxkit                     Start here to get Memorizer running in 10 minutes
│   │   ├── README.md                Tutorial to get started with Linuxkit
│   │   ├── memorizer.yml
│   │   └── run_memorizer.sh
│   ├── resources                    Resources for automate running memorizer
│   │   ├── initramfs                Build environment for initramfs
│   │   └── .ssh                     Public/Private key for login to debian image
│   ├── README.md                    Learn more about working with Memorizer source code
│   ├── memorizer.patch              Patch file for Linux Kernel 4.10.
│   ├── post-analysis                Python code used to analyze and parse data output by Memorizer.
│   │   └── CAPMAP.py

```

## Using Memorizer

In it's simplest form Memorizer can be built by patching a
vanilla Linux 4.10.0 kernel source with the Memorizer [patch
file](./src/memorizer.patch) and using one of our preset
kernel configurations. Instructions on how to configure
Memorizer and example files are in the
[src/KConfigs](./src/KConfigs). 

In this repo we have collected a few tools and resources to simplify getting a memorizer 
enviornment up quickly (with [run.sh](run.sh)) as well as
provide [instructions](src/evaluation/README.md) for
recreating the performance results from the SYSTOR'21 paper.
We are currently working on automating this process,
however, we have also provided the kernel images along with
a method for downloading the VM
([debian_image](https://hub.docker.com/layers/159698318/brucechien/memorizer/debian_image/images/sha256-49a699e21d156606dc6dd913d61f3625ddb70492d78b06d8680021207968f6a3?context=repo))
used to run the tests, permitting building from source and
using whichever virtualization method desired.

### Dependencies

- Docker

### Memorizer Development Approach

When building a kernel there are several options. 
A simple model that we use is to separately build a
nd pass the kernel image and the file system image 
separately to Qemu ([we use this tutorial](http://mgalgs.github.io/2015/05/16/how-to-build-a-custom-linux-kernel-for-qemu-2015-edition.html)), 
instead of installing Memorizer into the disk image a
nd building inside of that image. This allows for 
faster dev iterations because Memorizer is not an 
ideal env to hack on and build Memorizer.

```mermaid
graph TD
   subgraph "Kernel Compilation"
      kconf["Kernel Config"] --> comp["Compile"]
      ksrc["Kernel Source"] --> comp
   end
   subgraph "File System Generation"
      tmpfs["Init Ramdisk"] --> fsimg
      hdd["Prebuilt Debian Image"] --> fsimg
      hddcustom["Your Custom FS Image"] --> fsimg
   end
   subgraph "Qemu Boot"
      comp --> kimg["Kernel Image"]
      kimg --> Qemu
      fsimg["File System Image"] -->Qemu
      Qemu --> terminal
   end
```

Memorizer has been tested with GCC Versions: 5 and 6.

Each of these inputs can be customized. We've prepared a 
few common sets to make getting up and running quickly easy. 
We have also provided the same disk image we used for the 
evaluation in the SYSTOR'21 paper.

### Run Prebuilt Memorizer Kernel

The fatest way to run Memorizer. Run ```./run.sh```
([run.sh](run.sh)) and we provide an interactive mode to
guide you from building the kernel, root file system,
running Memorizer, collecting data to analyzing the data.
This script also allows the selection of one of the
evaluation kernel images, or even newly compiled ones. For
the fastest and lightest environment, and for the first run
we suggest you choose to boot into a shell, using linuxkit
source build, and the initramfs image. The script is great
but might not be robust and this script will allow for
faster failure modes.

The shell offers the options:

1. Automatic or boot to shell

2. Kernel image source: linuxkit-memorizer, source,
   systor-memorizer, systor-kasan, systor-linux. Each of the
   systor options uses the exact kernel `vmlinuz`'s used for
   the paper's evaluation.

3. Disk Image or Initramfs: you can select disk image and
   later pick which images to use, alternatively you can
   select the initramfs option which will dynamically build
   a ramdisk image for use (from
   [resources/initframf](resources/initramfs]. Note that the
   initramfs provides a nice environment that is fast and
   easy to explore. All evaluation comparisions should use
   the provide debeian disk image.

4. Select the image to use. You can choose to boot with any
   image, although we provide a default image that we used
   for our experimentation that will automatically download
   and upack.

5. QEMU config amount of ram: this is the amount of ram to
   give the VM. The next option will ask how much memory to
   give to memorizer by default.

6. QEMU Memorizer Memory: Memorizer uses it's own custom
   allocator. So it will preallocated this much memory from
   the kernel for it's internal use. Note these values are
   not checked (if you pick more here than you gave to QEMU
   all will be lost) and the amount of ram used by memorizer
   depends on the amount of tracing you do without manually
   freeing the data from the debugfs interface.

7. Num procs: how many virtual cpus to give QEMU. Suggested
   is to use one as more can run into performance slowdowns.

You should now be booting into Memorizer if you chose to
launch a shell. Note that the script is not completley
robust. So if your boot is taking a long time (>5 mins) then
likely something went wrong. For example, the script uses an
looping ssh command to busy wait till the system runs but if
you haven't accepted the host key then it will error.

If you selected automated testing you will get the following
prompts:

8. If you are in a memorizer kernel you can select a
   combination of tracing to perform from: allocation,
   memory accesses, calls, and stack access tracing. Tracing
   the stack has higher overheads and will take longer.

9. Select the command to run: type it into the prompt

At this point Memorizer an ssh command will be sent that
executes the test, including enabling, running, disabling,
and sending data out. The data output can take a signficant
time as the tool is serializing. This is true even for small
tests: for example, output for running `ls` takes
approximately 15-20 minutes if on the debian image.

Various options can take a while to boot and some of the
output is not indicative of the results. For the default
options (auto test using prebuilt images) five minutes is
long enough. Note that this tool is meant to automated a few
of the common options by selecting the kernel image, disk
image, and shell or automatic, which can all be build
manually and used by qemu.

#### Unstable Issues

The automated evaluation options do not work consistently.

Booting with KASAN-memorizer was inconsistent in booting,
please attempt a manual QEMU prompt.

#### Hints

- If it appears stalled while booting into a shell you can use `control-a c` the QEMU is not running.

### LinuxKit

One way to get started with Memorizer is running
[LinuxKit](https://github.com/linuxkit/linuxkit), which we
have include instructions for in
[./src/Linuxkit](./src/Linuxkit/). Follow the tutorial found
in the Linuxkit directory to get the kernel running in 10
minutes.

### [Resources](https://hub.docker.com/repository/docker/brucechien/memorizer)

- [debian_image](https://hub.docker.com/layers/159698318/brucechien/memorizer/debian_image/images/sha256-49a699e21d156606dc6dd913d61f3625ddb70492d78b06d8680021207968f6a3?context=repo): Pre-build debian iamge, username/password are as follows.
    - nick/nick
    - root/root

- [uscope](https://hub.docker.com/repository/registry-1.docker.io/brucechien/memorizer/tags?page=1&ordering=last_updated): Docker env for running uscope analysis.

A large data set collected on numerous benchmarks is available at [data set](https://drive.google.com/drive/folders/19tkGk6p5dIhIseYr35W_ZpLWNFVC_nO5?usp=sharing), which can be analyzed using the [Memorizer data parser](./src/post-analysis/CAPMAP.py). 

A specific tutorial on using this data set to analyze least-authority compartmentaliztion can be found on the [uSCOPE repo](https://gitlab.com/fierce-lab/uscope).

### [Contributing to Memorizer](https://gitlab.com/fierce-lab/linux-memorizer)

We welcome developers contributing to Memorizer by branching
from the
[linux-memorizer](https://gitlab.com/fierce-lab/linux-memorizer) repo and
creating a merge request to "memorizer" branch when finished. 
For development documentation please refer to this
[README](https://gitlab.com/fierce-lab/linux-memorizer/-/blob/memorizer/mm/memorizer/README.md).
Memorizer is open-sourced and released under
GPLv2 license.

### [Visualization Dashboard](https://fierce-lab.gitlab.io/memorizer/dashboard/index.html)

Visiualization tools for [Memorizer
data](https://drive.google.com/drive/folders/19tkGk6p5dIhIseYr35W_ZpLWNFVC_nO5?usp=sharing).
Memorizer visiualize memory accessing data to the following
graphs:
- [Flamegraphs](https://fierce-lab.gitlab.io/memorizer/dashboard/alloc.html)
- [Force directed graphs](https://fierce-lab.gitlab.io/memorizer/dashboard/filefdg.html)
- [Cycle histograms](https://fierce-lab.gitlab.io/memorizer/dashboard/call_cycle.html)
- [Sunburst](https://fierce-lab.gitlab.io/memorizer/dashboard/sunburst.html)
- [Heatmap](https://fierce-lab.gitlab.io/memorizer/dashboard/heatmap.html)
- [Top access rank](https://fierce-lab.gitlab.io/memorizer/dashboard/toprank.html)
