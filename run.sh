#!/bin/bash

# This script automate the following steps:
# 1. Memorizer kernel compilation
# 2. Roof file system
# 3. Booting memorizer kernel and root fs with QEMU
# 4. Background mode helps enable memorizer and run application to collect data or reproduce evaluation in the paper.
# 5. Shell mode will get the running QEMU shell
# Background mode allows users to get cmap results directly.
# Shell mode let users interact with the Memorizer QEMU env.

BASEDIR=`pwd`
SRC_DIR=$BASEDIR/src
LINUXKIT_DIR=$BASEDIR/src/Linuxkit
LINUXKIT_SRC=linuxkit-src
LINUXKIT_SRC_DIR=$LINUXKIT_DIR/$LINUXKIT_SRC
LINUXKIT_BIN_DIR=$LINUXKIT_SRC_DIR/bin
LINUXKIT_CMD=$LINUXKIT_BIN_DIR/linuxkit
RESOURCE_DIR=$BASEDIR/src/resources
SYSTOR_DIR=$BASEDIR/src/evaluation
INITRAMFS_DIR=$RESOURCE_DIR/initramfs
USCOPE_DATA_DIR=$RESOURCE_DIR/uscope_data
DEBIAN_PORT=$RANDOM
MEMORIZER_MEMORY=40

# Clone linux and patch memorizer, grab config, then compile
build_from_source()
{
    # TODO: make a container for the linux and env.
    cd $RESOURCE_DIR
    
    if [ -d "$LINUXKIT_DIR/build" ] && [ -f "$RESOURCE_DIR/memorizer-kernel" ]
    then 
        echo "Kernel already exists..."
    else

        if [ ! -d $RESOURCE_DIR/linux ]
        then 
            echo "Clone Linux source into $RESOURCE_DIR"
            git clone https://github.com/torvalds/linux.git
        fi

        cd linux
        git checkout c470abd4fde40ea6a0846a2beab642a578c0b8cd
        git apply $SRC_DIR/memorizer.patch
        cp $SRC_DIR/KConfigs/memorizer_config.config .config
        # TODO: how to check dependency of the .config file
        # make menuconfig
        echo "Compiling kernel..."
        make -j $(( $(nproc) + 1 ))
        
        cp arch/x86_64/boot/bzImage $RESOURCE_DIR/memorizer-kernel
        cp vmlinux $RESOURCE_DIR/memorizer-vmlinux
        echo "Output kernel and vmlinux to $RESOURCE_DIR"
        echo ""
    fi
    cd $BASEDIR
}

# example script for building memorizer from linuxkit
build_from_linuxkit()
{
    cd $LINUXKIT_DIR
    if [ ! -d "$LINUXKIT_DIR/linuxkit-src" ] 
    then 
        echo "Building linuxkit..."
        git clone https://github.com/linuxkit/linuxkit.git $LINUXKIT_SRC
        cd $LINUXKIT_SRC
        make
        cd ..
    fi

    if [ -d "$LINUXKIT_DIR/build" ] && [ -f "$RESOURCE_DIR/memorizer-kernel" ] && [ -f "$RESOURCE_DIR/memorizer-initrd.img" ]
    then 
        echo "Kernel already exists..."
    else
        echo "Building memorizer kernel using Linuxkit..."
        mkdir -p build
        $LINUXKIT_CMD build -dir build memorizer.yml 
        cp build/memorizer-kernel $RESOURCE_DIR
        cp build/memorizer-initrd.img $RESOURCE_DIR
        echo " nokaslr memalloc_size=$MEMORIZER_MEMORY" >> build/memorizer-cmdline
        cp build/memorizer-cmdline $RESOURCE_DIR
        echo "Output kernel and vmlinux to $RESOURCE_DIR"
        echo ""
    fi

    cd $BASEDIR
}

# example script for building init ramfs
build_initramfs()
{
    # TODO: build into docker env
    if [ ! -f $RESOURCE_DIR/initramfs.cpio.gz ]
    then
        cd $INITRAMFS_DIR
        echo "Building initramfs..."
        ruby build.rb  > /dev/null 2>&1
        cp $INITRAMFS_DIR/obj/busybox-x86/initramfs-busybox-x86.cpio.gz $RESOURCE_DIR/initramfs.cpio.gz
        echo "Build complete and output initramfs to $RESROUCE_DIR"
        echo ""
        cd $BASEDIR
    fi
}

# ssh into qemu instance
ssh_qemu()
{
    chmod 600 $RESOURCE_DIR/.ssh/id_rsa
    echo "Initial ssh test before looping..."
    ssh -o "StrictHostKeyChecking no"  -i $RESOURCE_DIR/.ssh/id_rsa root@localhost -p $DEBIAN_PORT "ls" #> /dev/null 2>&1
    while [ $? != 0 ]
    do
        ssh -o "StrictHostKeyChecking no" -i $RESOURCE_DIR/.ssh/id_rsa root@localhost -p $DEBIAN_PORT "ls" #> /dev/null 2>&1
    done
    echo "Finished ssh test, VM live"
}

# Enable memorizer for debian image
enable_debian_memorizer()
{
    echo "Memorizer tracing config (default is 3):"
    echo "1) Alloc"
    echo "2) Alloc + Access"
    echo "3) Alloc + Access + Call"
    echo "4) Alloc + Access + Stack-trace"
    echo "5) None"
    read option
    case $option in
        "1")
            ssh -i $RESOURCE_DIR/.ssh/id_rsa root@localhost -p $DEBIAN_PORT "echo 1 > /sys/kernel/debug/memorizer/memorizer_enabled"
            ;;
        "2")
            ssh -i $RESOURCE_DIR/.ssh/id_rsa root@localhost -p $DEBIAN_PORT "echo 1 > /sys/kernel/debug/memorizer/memorizer_enabled"
            ssh -i $RESOURCE_DIR/.ssh/id_rsa root@localhost -p $DEBIAN_PORT "echo 1 > /sys/kernel/debug/memorizer/memorizer_log_access"
            ;;
        "3"|"")
            ssh -i $RESOURCE_DIR/.ssh/id_rsa root@localhost -p $DEBIAN_PORT "echo 1 > /sys/kernel/debug/memorizer/memorizer_enabled"
            ssh -i $RESOURCE_DIR/.ssh/id_rsa root@localhost -p $DEBIAN_PORT "echo 1 > /sys/kernel/debug/memorizer/memorizer_log_access"
            ssh -i $RESOURCE_DIR/.ssh/id_rsa root@localhost -p $DEBIAN_PORT "echo 1 > /sys/kernel/debug/memorizer/cfg_log_on"
            ;;
        "4")
            ssh -i $RESOURCE_DIR/.ssh/id_rsa root@localhost -p $DEBIAN_PORT "echo 1 > /sys/kernel/debug/memorizer/memorizer_enabled"
            ssh -i $RESOURCE_DIR/.ssh/id_rsa root@localhost -p $DEBIAN_PORT "echo 1 > /sys/kernel/debug/memorizer/memorizer_log_access"
            ssh -i $RESOURCE_DIR/.ssh/id_rsa root@localhost -p $DEBIAN_PORT "echo 1 > /sys/kernel/debug/memorizer/stack_trace_on"
            ;;
        "5")
            ;;
        *)
            echo "Please enter a valid number..."
            exit 0
            ;;
    esac
}

# Disable memorizer for deiban iamge
disable_debian_memorizer()
{
    echo "Disable memorizer"
    echo ""
    ssh -i $RESOURCE_DIR/.ssh/id_rsa root@localhost -p $DEBIAN_PORT "echo 0 > /sys/kernel/debug/memorizer/memorizer_enabled"
    ssh -i $RESOURCE_DIR/.ssh/id_rsa root@localhost -p $DEBIAN_PORT "echo 0 > /sys/kernel/debug/memorizer/memorizer_log_access"
    ssh -i $RESOURCE_DIR/.ssh/id_rsa root@localhost -p $DEBIAN_PORT "echo 0 > /sys/kernel/debug/memorizer/cfg_log_on"
    ssh -i $RESOURCE_DIR/.ssh/id_rsa root@localhost -p $DEBIAN_PORT "echo 0 > /sys/kernel/debug/memorizer/stack_trace_on"
}

debian_output_kmap()
{
    local cmd_name=$1
    disable_debian_memorizer
    echo "Output kmap and cfgmap..."
    ssh -i $RESOURCE_DIR/.ssh/id_rsa root@localhost -p $DEBIAN_PORT "cat /sys/kernel/debug/memorizer/kmap > /root/$cmd_name-cmap"
    ssh -i $RESOURCE_DIR/.ssh/id_rsa root@localhost -p $DEBIAN_PORT "cat /sys/kernel/debug/memorizer/cfgmap >> /root/$cmd_name-cmap"
    echo "Output complete"
    echo ""

    if [ ! -d $USCOPE_DATA_DIR ]
    then
        mkdir $USCOPE_DATA_DIR
    fi

    echo "Output results to $USCOPE_DATA_DIR..."
    scp -P $DEBIAN_PORT -i $RESOURCE_DIR/.ssh/id_rsa root@localhost:/root/$cmd_name-cmap $USCOPE_DATA_DIR
    echo "Output complete"
    echo ""
}

# run ltp, Phoronix, or LMBench
run_application()
{
    if [ ! -d $USCOPE_DATA_DIR ]
    then
        mkdir $USCOPE_DATA_DIR
    fi

    enable_debian_memorizer
    prompt_command
    case $CMD in 
        "ltp")
            while :
            do
                echo "List LTP test cases or run LTP test case"
                echo "1) Run LTP test case"
                echo "2) List LTP test cases"
                read option
                if [[ $option == "1" || $option == "" ]]
                then
                    break
                fi
                # List all LTP test cases
                ssh -i $RESOURCE_DIR/.ssh/id_rsa root@localhost -p $DEBIAN_PORT "ls /opt/ltp/runtest"
                echo ""
            done
            read -p "Enter LTP test case to run: " ltp_test_suite
            ssh -i $RESOURCE_DIR/.ssh/id_rsa root@localhost -p $DEBIAN_PORT "/opt/ltp/runltp -f /opt/ltp/runtest/$ltp_test_suite"
            echo ""
            echo "LTP complete"
            echo ""

            debian_output_kmap $ltp_test_suite
            ;;
        "phoronix")
            while :
            do
                echo "List Phoronix test cases or run Phoronix test case"
                echo "1) Run Phoronix test case"
                echo "2) List Phoronix test cases"
                read option
                if [[ $option == "1" || $option == "" ]]
                then
                    break
                fi
                # List all Phoronix test cases
                ssh -i $RESOURCE_DIR/.ssh/id_rsa root@localhost -p $DEBIAN_PORT "phoronix-test-suite list-available-tests"
                echo ""
            done
            read -p "Enter Phoronix test suite to run: " phoronix_test_suite
            ssh -i $RESOURCE_DIR/.ssh/id_rsa root@localhost -p $DEBIAN_PORT "phoronix-test-suite install-test $phoronix_test_suite"
            ssh -i $RESOURCE_DIR/.ssh/id_rsa root@localhost -p $DEBIAN_PORT "phoronix-test-suite batch-run $phoronix_test_suite > /root/$phoronix_test_suite-result"
            echo ""
            echo "Phoronix complete"
            echo ""

            if [ ! -d $RESOURCE_DIR/phoronix_results ]
            then
                mkdir $RESOURCE_DIR/phoronix_results
            fi
            scp -P $DEBIAN_PORT -i $RESOURCE_DIR/.ssh/id_rsa root@localhost:/root/$phoronix_test_suite-result $RESOURCE_DIR/phoronix_results
            
            # Add an option for Phoronix to output cmap or not
            read -p "Choose to output cmap file [Y/N]: " option
            if [ $option == "Y" ]
            then
                debian_output_kmap $phoronix_test_suite
            fi
            ;;
        "lmbench")
            # TODO: run LMBench's process overhead and local bandwidth overhead test case only...
            echo "Run LMBench..."
            echo ""
            ssh -i $RESOURCE_DIR/.ssh/id_rsa root@localhost -p $DEBIAN_PORT "cd /home/nick/lmbench/ && make rerun"
            echo ""
            echo "LMBench complete"
            echo ""
            disable_debian_memorizer
            ssh -i $RESOURCE_DIR/.ssh/id_rsa root@localhost -p $DEBIAN_PORT "cd /home/nick/lmbench/ && make see"
            
            if [ ! -d $RESOURCE_DIR/lmbench_results ]
            then
                mkdir $RESOURCE_DIR/lmbench_results
            fi
            echo "Output LMBench results to $RESOURCE_DIR/lmbench_results/lmbench_summary..."
            echo ""
            scp -P $DEBIAN_PORT -i $RESOURCE_DIR/.ssh/id_rsa root@localhost:/home/nick/lmbench/results/summary.out $RESOURCE_DIR/lmbench_results/lmbench_summary
            ;;
        *)
            echo "Run $CMD..."
            echo ""
            ssh -i $RESOURCE_DIR/.ssh/id_rsa root@localhost -p $DEBIAN_PORT $CMD
            echo ""
            echo "$CMD complete"
            echo ""
            
            debian_output_kmap $CMD
            ;;
    esac    
}

# download prebuild iso image which has memorizer kernel and debian image
download_iso_image()
{
    # TODO: build iso image?
    return    
}

run_debian()
{
    local NETOPTS="-net nic,model=virtio,macaddr=52:54:00:12:34:56 -net user,hostfwd=tcp:127.0.0.1:$DEBIAN_PORT-:22"
    local FILEOPTS="-fsdev local,id=fs1,path=$RESOURCE_DIR,security_model=none -device virtio-9p-pci,fsdev=fs1,mount_tag=host-code"
    local GENOPTS="--enable-kvm -m ${QEMU_MEMORY}G $NETOPTS $FILEOPTS"

    qemu-system-x86_64 $GENOPTS \
        -kernel $KERNEL \
        -drive file=$ROOTFS,if=virtio \
        -smp $SMP \
        -append "root=/dev/vda1 console=hvc0 nokaslr memalloc_size=$MEMORIZER_MEMORY" \
        -chardev stdio,id=stdio,mux=on,signal=off \
        -device virtio-serial-pci \
        -device virtconsole,chardev=stdio \
        -mon chardev=stdio \
        -display none
}

run_debian_auto()
{
    local NETOPTS="-net nic,model=virtio,macaddr=52:54:00:12:34:56 -net user,hostfwd=tcp:127.0.0.1:$DEBIAN_PORT-:22"
    local FILEOPTS="-fsdev local,id=fs1,path=$RESOURCE_DIR,security_model=none -device virtio-9p-pci,fsdev=fs1,mount_tag=host-code"
    local GENOPTS="--enable-kvm -m ${QEMU_MEMORY}G $NETOPTS $FILEOPTS"

    qemu-system-x86_64 $GENOPTS \
        -kernel $KERNEL \
        -drive file=$ROOTFS,if=virtio \
        -smp $SMP \
        -append "root=/dev/vda1 console=hvc0 nokaslr memalloc_size=$MEMORIZER_MEMORY" \
        -display none \
        -daemonize
}

run_initramfs()
{
    local FILEOPTS="-fsdev local,path=$RESOURCE_DIR,id=fs1,security_model=none -device virtio-9p-pci,fsdev=fs1,mount_tag=host-code"
    local GENOPTS="--enable-kvm -m ${QEMU_MEMORY}G $FILEOPTS"

    qemu-system-x86_64 $GENOPTS \
        -kernel $KERNEL \
        -initrd $ROOTFS \
        -smp $SMP \
        -append "root=/dev/vda1 console=hvc0 nokaslr memalloc_size=$MEMORIZER_MEMORY" \
        -chardev stdio,id=stdio,mux=on,signal=off \
        -device virtio-serial-pci \
        -device virtconsole,chardev=stdio \
        -mon chardev=stdio \
        -display none
}

run_initramfs_auto()
{
    local FILEOPTS="-fsdev local,path=$RESOURCE_DIR,id=fs1,security_model=none -device virtio-9p-pci,fsdev=fs1,mount_tag=host-code"
    local GENOPTS="--enable-kvm -m ${QEMU_MEMORY}G $FILEOPTS"

    qemu-system-x86_64 $GENOPTS \
        -kernel $KERNEL \
        -initrd $ROOTFS \
        -smp $SMP \
        -append "root=/dev/vda1 console=hvc0 nokaslr memalloc_size=$MEMORIZER_MEMORY" \
        -display none \
        -daemonize
}

# Download debian image from docker hub
download_debian_image()
{
    if [ ! -f "$RESOURCE_DIR/debian.qcow2" ]
    then
        echo "Download debian image..."
        docker run --name tmp_debian brucechien/memorizer:debian_image /bin/true > /dev/null 2>&1
        docker cp tmp_debian:/home/debian.qcow2 $RESOURCE_DIR > /dev/null 2>&1
        docker rm tmp_debian > /dev/null 2>&1
        echo "Download complete and output to $RESOURCE_DIR"
        echo ""
    fi
}

# run memorizer with qemu
run_memorizer()
{
    cd $LINUXKIT_DIR
    case $FILESYS in 
        "initramfs")
            cd $RESOURCE_DIR
            echo "Booting initramfs..."
            if [ $SHELL_MODE == "true" ]
            then
                run_initramfs
            else
                run_initramfs_auto
                echo "Spinning on ssh to test for VM ready"
                ssh_qemu
                echo "Boot complete"
                echo ""
                run_application
            fi
            ;;
        "disk")
            echo "Booting memorizer kernel with image..."
            if [ $SHELL_MODE == "true" ]
            then
                run_debian
                #$LINUXKIT_CMD run qemu -mem ${QEMU_MEMORY}G -iso $ROOTFS memorizer
            else
                run_debian_auto
                echo "Spinning on ssh to test for VM ready"
                ssh_qemu
                echo "Boot complete"
                echo ""
                run_application
            fi
            ;;
    esac
    # Terminate the qemu instance
    pkill qemu-system-x86
    cd $BASEDIR
}

# run uscope analysis code
run_uscope()
{
    local uscope_dir="/home/uscope"
    local analysis_dir="$uscope_dir/analysis"
    local RAID_dir="$uscope_dir/RAID2021"
    local cluster_dir="$analysis_dir/cluster_output"
    local edge_dir="$analysis_dir/edge_assignment_results"
    local pdf_dir="$analysis_dir/output"

    if [ ! -d $USCOPE_DATA_DIR ]
    then
        mkdir $USCOPE_DATA_DIR
    fi

    if [ ! -d $RESOURCE_DIR/RAID2021 ]
    then
        echo "Download RAID2021 data..."
        docker run --name tmp_uscope brucechien/memorizer:uscope /bin/true > /dev/null 2>&1
        docker cp tmp_uscope:/home/uscope/RAID2021 $RESOURCE_DIR > /dev/null 2>&1
        docker rm tmp_uscope > /dev/null 2>&1
        echo "Download complete and output to $RESOURCE_DIR/RAID2021"
        echo ""
    fi

    echo "Run uscope analysis:"
    echo "1) Creating compartments"
    echo "2) Exploring the continuum"
    echo "3) Shell mode"
    read option
    
    case $option in
        "1")
            # Run DomainCreator and output result to host
            read -p "Input the vmlinux file to run analysis: " vmlinux
            read -p "Input the cmap file to run analysis: " cmap_file
            
            # Run sweep_edge_assignment and output result to host
            base_vmlinux=$(basename $vmlinux)
            base_cmap=$(basename $cmap_file)

            echo "Running compartment..."
            docker run --name tmp_compartment -td brucechien/memorizer:uscope > /dev/null 2>&1
            docker cp $vmlinux tmp_compartment:$analysis_dir/$base_vmlinux
            docker cp $cmap_file tmp_compartment:$analysis_dir/$base_cmap
            docker exec tmp_compartment python $analysis_dir/DomainCreator.py $analysis_dir/$base_vmlinux $analysis_dir/$base_cmap
            docker cp -a tmp_compartment:$cluster_dir  $USCOPE_DATA_DIR
            echo "Output compartment result to $USCOPE_DATA_DIR"
            
            docker stop tmp_compartment > /dev/null 2>&1
            docker rm tmp_compartment > /dev/null 2>&1
            ;;
        "2")
            read -p "Input the vmlinux file to run analysis: " vmlinux
            read -p "Input the cmap file to run analysis: " cmap_file
            
            # Run sweep_edge_assignment and output result to host
            base_vmlinux=$(basename $vmlinux)
            base_cmap=$(basename $cmap_file)

            echo "Running continum..."
            docker run --name tmp_continum -td brucechien/memorizer:uscope > /dev/null 2>&1
            docker cp $vmlinux tmp_continum:$analysis_dir/$base_vmlinux
            docker cp $cmap_file tmp_continum:$analysis_dir/$base_cmap
            docker exec tmp_continum python $analysis_dir/sweep_edge_assignment.py $analysis_dir/$base_vmlinux $analysis_dir/$base_cmap
            docker cp -a tmp_continum:$edge_dir  $USCOPE_DATA_DIR
            echo "Output continum result to $USCOPE_DATA_DIR"
            
            # Copy the pdf plot to host
            docker cp -a tmp_continum:$pdf_dir  $USCOPE_DATA_DIR
            echo "Output continum pdf graph to $USCOPE_DATA_DIR"
            echo ""
            docker stop tmp_continum > /dev/null 2>&1
            docker rm tmp_continum > /dev/null 2>&1
            ;;
        "3")
            docker run -it brucechien/memorizer:uscope bash
            ;;
        *)
            echo "Please enter valid number"
            exit 0
            ;;
    esac
}

# install software dependency
install_dependency()
{
    # TODO: install docker
    sudo apt-get install qemu
    sudo apt-get install linuxkit
    sudo apt-get install git
    sudo apt install ruby-full
}

# Check software dependency
check_dependency()
{
    if [ ! dpkg -s qemu >/dev/null 2>&1 ] 
    then
        echo "qemu not installed..."
        exit 0
    fi  
    if [ ! dpkg -s git >/dev/null 2>&1 ]
    then
        echo "git not installed..."
        exit 0
    fi  
    #if [ ! dpkg -s linuxkit >/dev/null 2>&1 ]
    if [ ! $LINUXKIT_CMD ]
    then
        echo "linuxkit not installed..."
        exit 0
    fi  
    if [ ! dpkg -s ruby-full >/dev/null 2>&1 ]
    then
        echo "ruby not installed..."
        exit 0
    fi  
    if [ ! dpkg -s docker >/dev/null 2>&1 ]
    then
        echo "docker not installed..."
        exit 0
    fi  
}

help()
{
    echo "Usage: ./run.sh -i debian -k linuxkit -c lmbench"
}

# Prompt for choosing root file system
prompt_rootfs()
{
    echo "Choose root file system:"
    echo "1) Disk image (ex: qcow2)"
    echo "2) Initramfs (ex : cpio.gz)"
    read rfs_option
    case $rfs_option in
        "1"|"")
            read -p "Please input the disk image file path: (default is src/resources/debian.qcow2)" ROOTFS
            ROOTFS=${ROOTFS:-$RESOURCE_DIR/debian.qcow2}
            echo ""
            FILESYS="disk"
            download_debian_image
            ;;
        "2")
            read -p "Please input the initramfs file path: (default is ./src/resources/initramfs.cpio.gz)" ROOTFS
            ROOTFS=${ROOTFS:-$RESOURCE_DIR/initramfs.cpio.gz}
            echo ""
            FILESYS="initramfs"
            build_initramfs
            ;;
        *)
            echo "Please enter a valid number"
            exit 0
            ;;
    esac
}

# Prompt for command or application to run in memorizer
prompt_command()
{
    if [ $SHELL_MODE == "false" ]
    then
        echo ""
        echo "Enter command to run, for example:"
        echo "* bash command"
        echo "* ltp"
        echo "* phoronix"
        echo "* lmbench"
        read -p ">" CMD
        echo ""
    fi
    CMD=${CMD:-ls}
}

prompt_mode()
{
    check_dependency

    echo "################################"
    echo "## Welcome to Memorizer Shell ##"
    echo "################################"
    echo ""
    # echo "Choose to run Memorizer or uscope"
    # echo "1) Memorizer"
    # echo "2) uSCOPE"
    # read option
    # case $option in
    #     "1"|"")
    #         echo ""
    #         ;;
    #     "2")
    #         echo "Run uscope to analyze data..."
    #         echo ""
    #         run_uscope
    #         exit 0
    #         ;;
    #     *)
    #         echo "Please enter a valid number..."
    #         exit 0
    #         ;;
    # esac
    echo "Choose to run in background or pop up a shell"
    echo "1) Background mode: automatic output cmap files and run uscope analysis"
    echo "2) Shell mode: Pop up shell for running Memorizer on QEMU"
    read option
    case $option in
        "1"|"")
            SHELL_MODE="false"
            ;;
        "2")
            SHELL_MODE="true"
            ;;
        *)
            echo "Please enter a valid number..."
            exit 0
            ;;
    esac
 
    echo "Choose Memorizer kernel build method:"
    echo "1) Build Memorizer kernel from Linuxkit"
    echo "2) Build Memorizer kernel from source code"
    echo "3) Use SYSTOR eval Memorizer kernel (src/evaluation/bzImages/Memorizer-kernel)"
    echo "4) Use SYSTOR eval KASAN kernel (src/evaluation/bzImages/KASAN-kernel)"
    echo "5) Use SYSTOR eval Linux kernel (src/evaluation/bzImages/Linux-kernel)"
    read option

    case $option in
        "1"|"")
            KERNEL=$RESOURCE_DIR/memorizer-kernel
            build_from_linuxkit
            prompt_rootfs
            ;;
        "2")
            KERNEL=$RESOURCE_DIR/memorizer-kernel
            build_from_source
            prompt_rootfs
            ;;
        "3")
            KERNEL=$SYSTOR_DIR/bzImages/Memorizer-kernel
            prompt_rootfs
            ;;
        "4")
            KERNEL=$SYSTOR_DIR/bzImages/KASAN-kernel
            prompt_rootfs
            ;;
        "5")
            KERNEL=$SYSTOR_DIR/bzImages/Linux-kernel
            prompt_rootfs
            ;;
        *)
            echo "Please enter valid number"
            exit 0
            ;;
    esac
    
    echo ""
    echo "Set QEMU boot configuration:"
    read -p "Set QEMU memory, default (64GB) press enter or enter a number (GB)" QEMU_MEMORY
    QEMU_MEMORY=${QEMU_MEMORY:-64}
    read -p "Set Memorizer memory, default (40GB) press enter or enter a number (GB)" MEMORIZER_MEMORY
    MEMORIZER_MEMORY=${MEMORIZER_MEMORY:-40}
    read -p "Set core numbers, default (single core) press enter or enter a number" SMP
    SMP=${SMP:-1}
    echo ""

    run_memorizer
}

prompt_mode
